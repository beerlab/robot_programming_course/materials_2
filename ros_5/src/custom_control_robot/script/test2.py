#!/usr/bin/env python3

import rospy

from move_base_msgs.msg import MoveBaseActionGoal


def main(args=None):
    rospy.init_node('test2')

    message_pub = rospy.Publisher("/robot/move_base/goal", MoveBaseActionGoal, queue_size=1)
    
    
    rate = rospy.Rate(30)
    while not rospy.is_shutdown():
    
        goal = MoveBaseActionGoal()

        goal.goal.target_pose.header.frame_id = 'robot_map'
        goal.goal.target_pose.pose.position.x = 0.0
        goal.goal.target_pose.pose.position.y = 4.0
        
        goal.goal.target_pose.pose.orientation.z = 1.0
        
        message_pub.publish(goal)
    
        rate.sleep()
    

if __name__ == '__main__':
    main()
